# Cordic Tanh Approximation

The CORDIC algorithm is a widely used tool for digital signal processing and image processing
applications. The hardware implementation of hyperbolic sine and cosine using CORDIC algorithm on
an ASIC is the main aim as the nature of the architecture can give enhanced speed at low cost with a lot of flexibility.
This can be attributed to the fact that it replaces multipliers with shifters and adders.
The algorithm  will be working on a hyperbolic rotation mode.

In hyperbolic mode, CORDIC is ran from iteration 1 instead of iteration 0 due to the fact atanh(1.0) is undefined. 
Furthermore, for hyperbolic equations the iterations (4, 13, 40......k, 3k+1) has to be repeated.